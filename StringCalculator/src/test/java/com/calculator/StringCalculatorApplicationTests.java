package com.calculator;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;

import org.assertj.core.api.SoftAssertions;
import org.junit.Rule;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.rules.ErrorCollector;
import org.springframework.boot.test.context.SpringBootTest;

import com.calculator.exceptions.NegativeNumberException;
import com.calculator.utils.StringCalculator;

@SpringBootTest
class StringCalculatorApplicationTests {

	StringCalculator stringCalculator = new StringCalculator();

	SoftAssertions soft = new SoftAssertions();

	@BeforeAll
	static void start() {
		System.out.println("started");
	}

	@Test
	void testAllRequirements() throws NegativeNumberException, IOException {

		assertEquals(0, stringCalculator.add(""));
		assertEquals(1, stringCalculator.add("1"));
		assertEquals(3, stringCalculator.add("1,2"));
		assertEquals(6, stringCalculator.add("1,2,3"));
		assertEquals(10, stringCalculator.add("1,2,3,4"));
		assertEquals(6, stringCalculator.add("1\n2,3"));
		assertEquals(38, stringCalculator.add("5\n2,5\n10,16"));

		assertEquals(3, stringCalculator.add("//;\n1;2"));
		assertEquals(6, stringCalculator.add("//?\n1?2?1?1?1"));
		assertEquals(3, stringCalculator.add("//;\n1;2"));
		assertEquals(6, stringCalculator.add("//%\n1%2%1%1%1"));

		assertEquals(6, stringCalculator.add("//%\n1%2%1%1%1"));

		assertThrows(NegativeNumberException.class, () -> {
			stringCalculator.add("//%\n-1%2%1%1%8");
		});
		assertThrows(NegativeNumberException.class, () -> {
			stringCalculator.add("//%\n1%-2%1%1%-8");
		});

		//you can uncomment this method for getCalledCount test
		// assertEquals(14, stringCalculator.getCalledCount());
		
		assertEquals(5, stringCalculator.add("//%\n1%2%1%1%1234"));

	}

	@AfterAll
	static void end() {

	}

}
