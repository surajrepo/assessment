package com.calculator.exceptions;

public class NegativeNumberException extends Exception {
	public NegativeNumberException(String errorMsg) {
		super(errorMsg);
	}
}
