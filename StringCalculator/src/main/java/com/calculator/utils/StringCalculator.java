package com.calculator.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import com.calculator.exceptions.NegativeNumberException;

public class StringCalculator {

	public int add(String numbers) throws NegativeNumberException, IOException {

		saveCalledCount();

		assert numbers != null;

		if (numbers.equals("")) {
			return 0;
		}

		String regularExpression = null;
		if (numbers.length() >= 2) {
			char[] charArray = numbers.toCharArray();
			if (numbers.contains("//")) {
				char defaultDelimiter = charArray[2];
				regularExpression = "[\\\\,\\\\n\\\\" + defaultDelimiter + "]+";
				String[] temp = numbers.split("\n");
				temp = temp[1].split(regularExpression);
				numbers = getString(temp);
			}
		}

		String strArray[] = numbers.split("[\\,\\n]+");
		int intNumbers[] = new int[strArray.length];

		for (int i = 0; i < strArray.length; i++) {
			intNumbers[i] = Integer.parseInt(strArray[i]);
		}
		validateInput(intNumbers);
		int sum = 0;
		for (int i = 0; i <= intNumbers.length - 1; i++) {
			sum += intNumbers[i];
		}
		return sum;

	}

	public int getCalledCount() throws IOException {
		FileReader reader = new FileReader("app.properties");
		Properties p = new Properties();
		p.load(reader);
		System.out.println(p.getProperty("calledcount"));
		return Integer.parseInt(p.getProperty("calledcount"));
	}

	private void saveCalledCount() throws IOException {
		Properties p = new Properties();
		File file = new File("app.properties");
		if (file.exists()) {
			FileReader reader = new FileReader("app.properties");
			p.load(reader);
			String count = p.getProperty("calledcount");
			int countToSave = Integer.parseInt(count);
			countToSave++;
			p.setProperty("calledcount", Integer.toString(countToSave));
			p.store(new FileWriter("app.properties"), "File to save called count");
		} else {
			p.setProperty("calledcount", Integer.toString(1));
			p.store(new FileWriter("app.properties"), "File to save called count");
		}
	}

	private String getString(String[] str) {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < str.length; i++) {
			stringBuilder.append(str[i] + ",");
		}
		String temp = stringBuilder.toString();
		return temp.substring(0, temp.length() - 1);
	}

	private void validateInput(int[] intArray) throws NegativeNumberException {
		String negativeNumbers = new String();
		int negativeCount = 0;
		for (int i = 0; i < intArray.length; i++) {
			if (intArray[i] < 0) {
				negativeCount++;
				negativeNumbers += intArray[i] + ",";
			}
			if (intArray[i] > 100) {
				intArray[i] = 0;
			}
		}
		System.out.println("negativeNumbers.toString() " + negativeNumbers.toString());
		if (negativeCount > 0) {
			throw new NegativeNumberException(
					"negatives not allowed " + negativeNumbers.substring(0, negativeNumbers.length() - 1));
		}
	}

}
